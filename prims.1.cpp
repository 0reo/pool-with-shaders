#include "prims.h"
#include <math.h>
#include <GL/glut.h>
#include <stdio.h>
//#include <glut.h>
prims::prims(){}
prims::~prims(){}

void prims:: drawLine(float p1[2], float p2[2], float c1[2], float c2[3])
{
	glBegin(GL_LINES);
		glColor3fv(c1);
		glVertex2fv(p1);
		glColor3fv(c2);
		glVertex2fv(p2);
	glEnd();
}
void prims:: drawQuad(float p1[2], float p2[2], float p3[2], float p4[2], float c[3])
{
	glBegin(GL_LINES);
		glColor3fv(c);
		glVertex2fv(p1);
		glVertex2fv(p2);
	glEnd();
	glBegin(GL_LINES);
		glVertex2fv(p2);
		glVertex2fv(p3);
	glEnd();
	glBegin(GL_LINES);
		glVertex2fv(p3);
		glVertex2fv(p4);
	glEnd();
	glBegin(GL_LINES);
		glVertex2fv(p4);
		glVertex2fv(p1);
	glEnd();
}
void prims:: drawRectangle(float cen[2], float w, float h)
{
	glBegin(GL_QUADS);
		glColor3f(0.0,1.0,0.0);
		glVertex2f(cen[0]+(w/2.0), cen[1]+(h/2.0));
		glVertex2f(cen[0]+(w/2.0), cen[1]-(h/2.0));
		glVertex2f(cen[0]-(w/2.0), cen[1]-(h/2.0));
		glVertex2f(cen[0]-(w/2.0), cen[1]+(h/2.0));
	glEnd();
}
void prims::drawCircle(float cen[2], float r)
{
int i = 1;
double x,y;
x = cen[0]+r;
y = cen[1];

			glBegin(GL_LINE_LOOP);//GL_LINE_LOOP keeps the line solid, but it doesn't look as cool
		glColor3f(0.0,1.0,0.0);
	while(i <= 360)
		{

			glVertex2f(x,y);
			x = cen[0] + r*sin(i);
			y = cen[1] + r*cos(i);
			i++;
		}
					glEnd();

}


void prims::drawBox(float cen[2], float width, float height, float depth, float normal[][3])
{

	glBegin(GL_POLYGON);
		///front
		//glNormal3f(0.0, 0.0, 1.0);
        glNormal3fv(normal[4]);

		glVertex3f(cen[0]+(width/2.0), cen[1]+(height/2.0), (depth/2.0));
		glVertex3f(cen[0]+(width/2.0), cen[1]-(height/2.0), (depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]-(height/2.0), (depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]+(height/2.0), (depth/2.0));

	glEnd();

	   	glBegin(GL_POLYGON);
		///back
        //glNormal3f(0.0, 0.0, -1.0);
        glNormal3fv(normal[0]);

		glVertex3f(cen[0]+(width/2.0), cen[1]+(height/2.0), -(depth/2.0));
		glVertex3f(cen[0]+(width/2.0), cen[1]-(height/2.0), -(depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]-(height/2.0), -(depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]+(height/2.0), -(depth/2.0));
	glEnd();

    glBegin(GL_POLYGON);
		//glNormal3f(0.0, 1.0, 0.0);
        glNormal3fv(normal[3]);
		//glColor3f(0.0,1.0,0.0);
///top
		glVertex3f(cen[0]+(width/2.0), cen[1]+(height/2.0), (depth/2.0));
		glVertex3f(cen[0]+(width/2.0), cen[1]+(height/2.0), -(depth/2.0));
		glVertex3f(cen[0]+(width/2.0), cen[1]-(height/2.0), -(depth/2.0));
		glVertex3f(cen[0]+(width/2.0), cen[1]-(height/2.0), (depth/2.0));//back depth = +
	glEnd();



   	glBegin(GL_POLYGON);
		///bottom
		//glNormal3f(0.0, -1.0, 0.0);
        glNormal3fv(normal[2]);

		glVertex3f(cen[0]-(width/2.0), cen[1]+(height/2.0), (depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]-(height/2.0), (depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]-(height/2.0), -(depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]+(height/2.0), -(depth/2.0));
	glEnd();

   	glBegin(GL_POLYGON);
		///right
				//glNormal3f(1.0, 0.0, 0.0);
                glNormal3fv(normal[5]);


		glVertex3f(cen[0]+(width/2.0), cen[1]+(height/2.0), (depth/2.0));
		glVertex3f(cen[0]+(width/2.0), cen[1]+(height/2.0), -(depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]+(height/2.0), -(depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]+(height/2.0), (depth/2.0));
	glEnd();

   	glBegin(GL_POLYGON);
        ///left
        //glNormal3f(-1.0, 0.0, 0.0);
        glNormal3fv(normal[1]);

		glVertex3f(cen[0]+(width/2.0), cen[1]-(height/2.0), (depth/2.0));
		glVertex3f(cen[0]+(width/2.0), cen[1]-(height/2.0), -(depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]-(height/2.0), -(depth/2.0));
		glVertex3f(cen[0]-(width/2.0), cen[1]-(height/2.0), (depth/2.0));
	glEnd();




}
