#include <GLee.h>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include "prims.h"
#include "textfile.h"
#include <stdio.h>

#define STRIPED 1
#define SOLID 2
#define TABLE 3
#define RED 0
#define GREEN 1
#define BLUE 2
#define YELLOW 3

prims shapes;
float cen[2] = {10.0, 0.0};
GLuint p, f, v;
GLint ball, color, lightPos, lightPos2;
GLdouble x, y = 1.0;
GLdouble incX = 0.1;
GLdouble incY = 0.1;
GLdouble z = -50.0;

float lp[3] = {0.0, 0.0, 0.0};
float lp2[3] = {0.0, 0.0, 0.0};
float normals[][3]={{0,0,-1},{0,1,0},{-1,0,0},{1,0,0},{0,0,1},{0,-1,0}};
/* GLUT callback Handlers */


static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-ar, ar, -1.0, 1.0, 2.0, 100.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}

static void display(void)
{
    const float t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    const float a = t*90.0;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
    glTranslated(0.0, 0.0, 4.0);
        glPushMatrix();

            glUniform1i(ball, TABLE);
            glTranslated(-10.0, 0.0, z);
            glColor3f(1.0, 0.0, 0.0);
            shapes.drawBox(cen, 60, 30, 2.0, normals);
            //drawCube();
        glPopMatrix();

        glPushMatrix();
            glUniform1i(ball, SOLID);
            glUniform1i(color, BLUE);
            glTranslated(x, y, -49.0);
            glRotated(a, 1, 1, 0);
            glutSolidSphere(1, 20, 20);
        glPopMatrix();

        glPushMatrix();
            glUniform1i(ball, STRIPED);
            glUniform1i(color, RED);
            glTranslated(x, -y, -49.0);
            glRotated(a, 1, 1, 0);
            glutSolidSphere(1, 20, 20);
        glPopMatrix();

        glPushMatrix();
            glUniform1i(ball, SOLID);
            glUniform1i(color, YELLOW);
            glTranslated(-x, y, -49.0);
            glRotated(a, 1, 1, 0);
            glutSolidSphere(1, 20, 20);
        glPopMatrix();

        glPushMatrix();
            glUniform1i(ball, STRIPED);
            glUniform1i(color, GREEN);
            glTranslated(-x, -y, -49.0);
            glRotated(a, 1, 1, 0);
            glutSolidSphere(1, 20, 20);
        glPopMatrix();

    glPopMatrix();

    glUniform3fv(lightPos, 1,  lp);
    glUniform3fv(lightPos2, 1, lp2);

    if (x >= 27.5 || x <= -27.5)
        incX *= -1.0;
    if (y >= 12.5 || y <= 0.0)
        incY *= -1.0;

    x+=incX/10.0;
    y+=incY/10.0;
    glutSwapBuffers();
}


static void key(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 27 :
        case 'q':
            exit(0);
        break;

        case '+':
            lp[0] += 1.0;
            lp2[0] -= 1.0;
        break;

        case '-':
            lp[0] -= 1.0;
            lp2[0] += 1.0;
        break;
    }

    glutPostRedisplay();
}

static void idle(void)
{
    glutPostRedisplay();
}



void printShaderInfoLog(GLuint obj)
{
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;

    glGetShaderiv(obj, GL_INFO_LOG_LENGTH,&infologLength);

    if (infologLength > 0)
    {
        infoLog = (char *)malloc(infologLength);
        glGetShaderInfoLog(obj, infologLength, &charsWritten, infoLog);
        printf("%s\n",infoLog);
        free(infoLog);
    }
}

void printProgramInfoLog(GLuint obj)
{
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;

    glGetProgramiv(obj, GL_INFO_LOG_LENGTH,&infologLength);

    if (infologLength > 0)
    {
        infoLog = (char *)malloc(infologLength);
        glGetProgramInfoLog(obj, infologLength, &charsWritten, infoLog);
        printf("%s\n",infoLog);
        free(infoLog);
    }
}


void setShaders()
{

    char *vs = NULL,*fs = NULL;

    v = glCreateShader(GL_VERTEX_SHADER);
    f = glCreateShader(GL_FRAGMENT_SHADER);

    vs = textFileRead("../../shaders/pool.vert");
    fs = textFileRead("../../shaders/pool.frag");

    const char * ff = fs;
    const char * vv = vs;

    glShaderSource(v, 1, &vv, NULL);
    glShaderSource(f, 1, &ff, NULL);

    free(vs);
    free(fs);

    glCompileShader(v);
    glCompileShader(f);
    printShaderInfoLog(f);

    p = glCreateProgram();

    glAttachShader(p,f);
    glAttachShader(p,v);

    glLinkProgram(p);
    printProgramInfoLog(p);
    glUseProgram(p);

    ball = glGetUniformLocation(p, "object");
        printf("\nhere\n");

    color = glGetUniformLocation(p, "col");
    lightPos = glGetUniformLocation(p, "lightPos");
    lightPos2 = glGetUniformLocation(p, "lightPos2");
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(800,600);
    glutInitWindowPosition(500,250);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("GLUT Shapes");

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glutIdleFunc(idle);


    glEnable(GL_DEPTH_TEST);
    glEnable(GL_POLYGON_SMOOTH);
    glDepthFunc(GL_LESS);

    setShaders();
    glutMainLoop();

    return EXIT_SUCCESS;
}
