// rubik.fs
//
// Longitudinal stripes, end caps

uniform int uni;
varying vec3 V; // object-space position
varying vec3 N; // eye-space normal
varying vec3 L; // eye-space light vector

const vec3 color []= 
{vec3(1.0, 0.0, 0.0),
vec3(0.0, 1.0, 0.0),
vec3(0.0, 0.0, 1.0),
vec3(1.0, 1.0, 0.0),
vec3(1.0, 1.0, 1.0),
vec3(0.9, 0.5, 0.0)};

const vec3 myRed = vec3(1.0, 0.0, 0.0);
const vec3 myYellow = vec3(1.0, 1.0, 0.0);
const vec3 myGreen = vec3(0.0, 1.0, 0.0);
const vec3 myBlue = vec3(0.0, 0.0, 1.0);
const vec3 myWhite = vec3(1.0, 1.0, 1.0);
const vec3 myBlack = vec3(0.0, 0.0, 0.0);

const float smoothEdgeTol = 0.005;
const float ambientLighting = 0.2;
const float specularExp = 60.0;
const float specularIntensity = 0.75;

void main (void)
{
    // Normalize vectors
    vec3 NN = normalize(N);
    vec3 NL = normalize(L);
    vec3 NH = normalize(NL + vec3(0.0, 0.0, 1.0));
    vec3 NV = normalize(V);

    // coloured sides
    vec3 surfColor;
    int row, column, index;

    if (V.x <-29.5){
	if (uni) surfColor = color[0];
	else
	{
	    row = (V.y + 30) / 20; 
	    column = (V.z + 30) / 20; 
	    index = (+row*row+ column*column) %6;
	    if (index =0) surfColor = color[0]; 
	    if (index =1) surfColor = color[1]; 
	    if (index =2) surfColor = color[2]; 
	    if (index =3) surfColor = color[3]; 
	    if (index =4) surfColor = color[4]; 
	    if (index =5) surfColor = color[5]; 
	}
	if(fract ((V.z + 30) / 20) <0.07|| fract ((V.y + 30) / 20) <0.07) 			surfColor=myBlack;
    }

    if (V.x >29.5){
	if (uni) surfColor = color[1];
        else
	{
	    row = (V.y + 30) / 20; 
	    column = (V.z + 30) / 20; 
	    index = (1+row*row+ column*column) %6;
	    if (index ==0) surfColor = color[0]; 
	    if (index ==1) surfColor = color[1]; 
	    if (index ==2) surfColor = color[2]; 
	    if (index ==3) surfColor = color[3]; 
	    if (index ==4) surfColor = color[4]; 
	    if (index ==5) surfColor = color[5]; 
	}
	if(fract ((V.z + 30) / 20) <0.07|| fract ((V.y + 30) / 20) <0.07) 			surfColor=myBlack;
    }

    if (V.y <-29.5){
	if (uni) surfColor = color[2];
        else
	{
	    row = (V.x + 30) / 20; 
	    column = (V.z + 30) / 20; 
	    index = (2+row*row+ column*column) %6;
	    if (index ==0) surfColor = color[0]; 
	    if (index ==1) surfColor = color[1]; 
	    if (index ==2) surfColor = color[2]; 
	    if (index ==3) surfColor = color[3]; 
	    if (index ==4) surfColor = color[4]; 
	    if (index ==5) surfColor = color[5]; 
	}
	if(fract ((V.x + 30) / 20) <0.07|| fract ((V.z + 30) / 20) <0.07) 			surfColor=myBlack;
    }

    if (V.y >29.5){
	if (uni) surfColor = color[3];
        else
	{
	    row = (V.x + 30) / 20; 
	    column = (V.z + 30) / 20; 
	    index = (3+row*row+ column*column) %6;
	    if (index ==0) surfColor = color[0]; 
	    if (index ==1) surfColor = color[1]; 
	    if (index ==2) surfColor = color[2]; 
	    if (index ==3) surfColor = color[3]; 
	    if (index ==4) surfColor = color[4]; 
	    if (index ==5) surfColor = color[5]; 
	}
	if(fract ((V.x + 30) / 20) <0.07|| fract ((V.z + 30) / 20) <0.07) 			surfColor=myBlack;
    }

    if (V.z <-29.5){
	if (uni) surfColor = color[4];
        else
	{
	    row = (V.x + 30) / 20; 
	    column = (V.y + 30) / 20; 
	    index = (4+row*row+ column*column) %6;
	    if (index ==0) surfColor = color[0]; 
	    if (index ==1) surfColor = color[1]; 
	    if (index ==2) surfColor = color[2]; 
	    if (index ==3) surfColor = color[3]; 
	    if (index ==4) surfColor = color[4]; 
	    if (index ==5) surfColor = color[5]; 
	}
	if(fract ((V.x + 30) / 20) <0.07|| fract ((V.y + 30) / 20) <0.07) 			surfColor=myBlack;
    }

    if (V.z >29.5) {
	if (uni) surfColor = color[5];
        else
	{
	    row = (V.x + 30) / 20; 
	    column = (V.y + 30) / 20; 
	    index = (5+row*row+ column*column) %6;
	    if (index ==0) surfColor = color[0]; 
	    if (index ==1) surfColor = color[1]; 
	    if (index ==2) surfColor = color[2]; 
	    if (index ==3) surfColor = color[3]; 
	    if (index ==4) surfColor = color[4]; 
	    if (index ==5) surfColor = color[5]; 
	}
	if(fract ((V.x + 30) / 20) <0.07|| fract ((V.y + 30) / 20) <0.07) 			surfColor=myBlack;
    }


    // calculate diffuse lighting + 20% ambient
    surfColor *= (ambientLighting + vec3(max(0.0, dot(NN, NL))));

    // calculate specular lighting w/ 75% intensity
    surfColor += (specularIntensity * 
        vec3(pow(max(0.0, dot(NN, NH)), specularExp)));

    gl_FragColor = vec4(surfColor, 1.0);
}
