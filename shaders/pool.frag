varying vec3 v_V;
varying vec3 v_N;
varying vec3 L;
varying vec4 vEye;

uniform int object;
uniform vec3 lightPos, lightPos2;
uniform int col;

float LightIntensity[2];
vec3 V;
vec3 surfaceColor;

uniform struct Light
{
	uniform vec3 lightSource;
	uniform vec3 lightDirection;
	uniform vec3 reflectVec;
	uniform float diffuse;
	uniform vec3 lightVec;
	uniform vec3 viewVec;
	uniform float ammount;
}_Light;

Light l[2];

void table(vec3 pos)///sets surface color for pool table
{
    surfaceColor.r = (0.0);
	surfaceColor.g = (0.5);
	surfaceColor.b = (0.003);

	if (((pos.y >= -15.5 && pos.y <= -13.5) || (pos.x >= -20.5 && pos.x <= -18.5)) ||
		((pos.y <= 15.5 && pos.y >= 13.5) || (pos.x <= 40.5 && pos.x >= 38.5)))
	{
		surfaceColor.r = (0.5);
		surfaceColor.g = (0.0);
		surfaceColor.b = (0.003);
	}


}

void stripedCueBall(vec3 pos)
{
	pos = normalize(pos);
			surfaceColor.r = (1.0);
		surfaceColor.g = (0.8);
		surfaceColor.b = (0.6);
	if (pos.y >= -0.7 && pos.y <= 0.7)
	{
        surfaceColor.r = (0.0);
        surfaceColor.g = (0.0);
        surfaceColor.b = (0.0);
		if (col == 0)
			surfaceColor.r = (1.0);
		if (col == 1)
			surfaceColor.g = (1.0);
		if (col == 2)
			surfaceColor.b = (1.0);
		if (col == 3)
		{
			surfaceColor.r = (1.0);
			surfaceColor.g = (1.0);
		}
	}

}

void solidCueBall()
{
	surfaceColor.r = (0.0);
	surfaceColor.g = (0.0);
	surfaceColor.b = (0.0);
	if (col == 0)
		surfaceColor.r = (1.0);
	if (col == 1)
		surfaceColor.g = (1.0);
	if (col == 2)
		surfaceColor.b = (1.0);
	if (col == 3)
	{
		surfaceColor.r = (1.0);
		surfaceColor.g = (1.0);
	}
}

void main()
{
	l[0].lightSource = lightPos;
	l[1].lightSource = lightPos2;
	float spec[2];///specular light(highlight)

		if (object == 1)
		{
			stripedCueBall(v_V);
		}
		else if (object == 2)
		{
			solidCueBall();
		}
		if (object == 3)
		{
			table(v_V);
		}



	for (int i = 0; i < 2; i++)
	{
		spec[i]    = 0.0;
		l[i].lightDirection  = normalize(l[i].lightSource - vEye.xyz);// L
		l[i].reflectVec = normalize(l[i].lightDirection + vec3(0.0, 0.0, 1.0)); //R
		l[i].diffuse = max(dot(l[i].lightDirection, v_N), 0.0);

   		l[i].viewVec = normalize(v_N.xyz);//V

		if (l[i].diffuse > 0.0)
		{
        		spec[i] = max(dot(l[i].reflectVec, l[i].viewVec), 0.0);
        		spec[i] = pow(spec[i], 120.0);
		}

		    surfaceColor *=  l[i].diffuse;
                if (object != 3)
                {
                    surfaceColor += spec[i];
                }
    }

   	gl_FragColor = vec4 (surfaceColor, 1.0);

}
