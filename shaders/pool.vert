varying vec3 v_V;
varying vec3 v_N;
varying vec3 L;
varying vec3 colorC[2];
varying vec4 vEye;

void main()
{
	v_V = gl_Vertex.xyz;
	v_N = gl_NormalMatrix * gl_Normal;

	vEye = gl_ModelViewMatrix * gl_Vertex;
	L = vec3(0.0, 0.0, 0.0);

	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

}
